const express = require("express");
const router = express.Router();
const { check, validationResult } = require('express-validator');
const auth = require('../../middleware/auth');


//MODELS
const Post = require('../../models/Post');
const Profile = require('../../models/Profile');
const User = require('../../models/User');



// @router      POST api/posts
// @desc        Create a post from a user 
// @access      Private: needs token: only user with id can send post
router.post("/", [auth, [
    //check for text input as second middleware
    check('text', 'Text is required')
        .not()
        .isEmpty()
]], async (req, res) => {

    //error check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    try {

        //grab the user from the request by their ID (we can use req.user because the token is sent! This is their ID!)
        const user = await User.findById(req.user.id).select('-password');

        //create new post using the Post model we passed in!
        const newPost = new Post({
            text: req.body.text,
            name: user.name,
            avatar: user.avatar,
            user: req.user.id
        });

        //save the post
        const post = await newPost.save();

        //send the response
        res.json(post)


    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});



// @router      GET api/posts
// @desc        Get all posts
// @access      Private: needs token: we should not be able to see posts unless you are logged in (good logic for a blog web app too!)
router.get('/', auth, async (req, res) => {
    try {
        //get all posts, sorted by date from the most recent (use -1 see JS docs on sort())
        const posts = await Post.find().sort({ date: -1 });
        res.json(posts);

    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});




// @router      GET api/posts/:id
// @desc        Get post by ID (the post id not the users id!)
// @access      Private: needs token: we should not be able to see posts unless you are logged in (good logic for a blog web app too!)
router.get('/:id', auth, async (req, res) => {
    try {
        //get post by the POSTS id passed in the req (url)
        const post = await Post.findById(req.params.id);


        //check if the ID even exists
        if (!post) {
            return res.status(404).json({ msg: 'Post not found' });
        }

        res.json(post);

    } catch (err) {
        console.error(err.message);
        //bad format id (no id, one letter, etc.)
        if (err.kind === 'ObjectId') {
            return res.status(404).json({ msg: 'Post not found' });
        }
        res.status(500).send('Server Error');
    }
});





// @router      DELETE api/posts/:id
// @desc        Delete a post by a post ID
// @access      Private: needs token: we should not be able to see posts unless you are logged in (good logic for a blog web app too!)
router.delete('/:id', auth, async (req, res) => {
    try {
        //get post to delete by the id in the request
        const post = await Post.findById(req.params.id);

        //if the post does not exits
        if (!post) {
            return res.status(404).json({ msg: 'Post not found' }); //404 === not found
        }

        //validate user deleting post, owns this post
        if (post.user.toString() !== req.user.id) {
            return res.status(401).json({ msg: 'User not Authorized' });    //401 === not authorized    
        }

        //ELSE, remove the post since it checks out: (remove() is array method, and remember the posts is an array of objects so this is valid)
        await post.remove()

        //output confirmation and the post contents
        res.json({ msg: 'Post Removed' });

    } catch (err) {
        console.error(err.message);
        //bad format id (no id, one letter, etc.)
        if (err.kind === 'ObjectId') {
            return res.status(404).json({ msg: 'Post not found' });
        }
        res.status(500).send('Server Error');       // 500 === server error
    }
});



// @router      PUT api/posts/:id
// @desc        Like a post by its ID
// @access      Private: needs token: so pass auth
router.put('/like/:id', auth, async (req, res) => {
    try {

        //get the post by the ID in the url (SIMPLE!)
        const post = await Post.findById(req.params.id);

        //check if the post has been liked (preventinfinite likes) compare the like count (0 or not at this index) return a response to not allow
        if (post.likes.filter(like => like.user.toString() === req.user.id).length > 0) {
            return res.status(400).json({ msg: 'Post already liked' }); //400 = bad request
        }

        //else, no likes so add a like to this index via this user
        post.likes.unshift({ user: req.user.id });

        //save
        await post.save();

        //respond sending total likes
        res.json(post.likes);

    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});




// @router      PUT api/posts/:id
// @desc        UN-Like a post by its ID
// @access      Private: needs token: so pass auth
router.put('/unlike/:id', auth, async (req, res) => {
    try {

        //get the post by the ID in the url (SIMPLE!)
        const post = await Post.findById(req.params.id);

        //check if the post has NOT been liked [similar to the above put, but in this case, not liked, not if it was] (preventinfinite likes) compare the like count (0 or not at this index) return a response to not allow
        if (post.likes.filter(like => like.user.toString() === req.user.id).length === 0) {
            return res.status(400).json({ msg: 'Post has not been liked' }); //400 = bad request
        }

        //get remove index via the id
        const removeIndex = post.likes.map(like => like.user.toString()).indexOf(req.user.id);

        //remove it
        post.likes.splice(removeIndex, 1);


        //save
        await post.save();

        //respond sending total likes
        res.json(post.likes);

    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});





// @router      POST api/posts/comment/:id
// @desc        Comment on a post via the posts ID
// @access      Private: needs token: only user with id can send post
router.post("/comment/:id", [auth, [
    //check for text input as second middleware
    check('text', 'Text is required')
        .not()
        .isEmpty()
]], async (req, res) => {

    //error check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    try {

        //grab the user from the request by their ID (we can use req.user because the token is sent! This is their ID!)
        const user = await User.findById(req.user.id).select('-password');

        const post = await Post.findById(req.params.id);

        //create new post using the Post model we passed in!
        const newComment = new Post({
            text: req.body.text,
            name: user.name,
            avatar: user.avatar,
            user: req.user.id
        });

        //add to the comments on this post
        post.comments.unshift(newComment);

        //save
        await post.save();

        //send the response
        res.json(post)


    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});



// @router      DELETE api/posts/comment/:id/:comment_id
// @desc        Delete comment: takes in the posts id and then the comments specific id to delete
// @access      Private: needs token: only user with id can send post
router.delete('comment/:id/:comment_id', auth, async (req, res) => {
    try {

        //get the post
        const post = await Post.findById(req.params.id);

        //pull out the comment on this post
        const comment = post.comments.find(comments => {
            comment.id === req.params.comment_id;
        });

        //make sure the comment exists
        if (!comment) {
            return res.status(404).json({ msg: 'Comment does not exist' });
        }

        //check user
        if (comment.user.toString() !== req.user.id) {
            return res.status(401).json({ msg: 'User not authorized' });
        }

        //if all is okay, find the comment index and delete it
        //get remove index via the id
        const removeIndex = post.likes.map(comment => comment.user.toString()).indexOf(req.user.id);

        post.comments.splice(removeIndex, 1);

        //save
        await post.save();

        //send the comments
        res.send(comments);


    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});




//CHALLENGES:
/**
 *  1) Add a like to a comment on a post
 *  2) Update a comment (edit feature)
 */

module.exports = router;