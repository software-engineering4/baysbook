const mongoose = require('mongoose');
const Schema = mongoose.Schema;                     //cleaner: put the schema method into a variable -> you will see why below


//post schema (data model for the DB is basically what a schema is fyi)
const PostSchema = new Schema({
    //connect a user to a post (with their userID)
    user: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    //text post field
    text: {
        type: String,
        required: true
    },
    //the name of the user
    name: {
        type: String,
    },
    avatar: {
        type: String,
    },
    likes: [
        {
            //array of user id's of the likes on a post: restricts multiple likes by one person
            user: {
                type: Schema.Types.ObjectId,
                ref: 'users'
            }
        }
    ],
    comments: [
        {
            //array of user id's of the likes on a post: restricts multiple likes by one person
            user: {
                type: Schema.Types.ObjectId,
                ref: 'users'
            },
            text: {
                type: String,
                required: true
            },
            //see challenge for posts route line 208 of posts.js (this likes array will allow us to hold likes on a comment and be able to target that like via its id)
            likes: [
                {
                    type: Schema.Types.ObjectId,
                    ref: 'users'
                }
            ],
            name: {
                type: String,
            },
            avatar: {
                type: String,
            },
        }
    ],
    date: {
        type: Date,
        default: Date.now
    }
});

//and exort this module as mongoose data model called Posts
module.exports = Posts = mongoose.model('post', PostSchema);