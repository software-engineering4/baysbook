const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const { check, validationResult } = require("express-validator");
const request = require('request');
const config = require('config');

const Profile = require("../../models/Profile");
const User = require("../../models/User");




// @router GET api/profile/me
// @desc Get current user's profile
// @access Private (Only when a token is sent)
router.get("/me", auth, async (req, res) => {
  try {
    const profile = await Profile.findOne({
      user: req.user.id
    }).populate("user", ["name", "avatar"]);
    if (!profile) {
      return res.status(400).json({ msg: "There is no profile for this user" });
    }
    res.json(profile);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});




// @router POST api/profile
// @desc Create or update user profile
// @access Private [needs token in header]
router.post(
  "/",
  [
    auth,
    [
      check("status", "Status is required")
        .not()
        .isEmpty(),
      check("skills", "Skills is required")
        .not()
        .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array });
    }

    //destructuring (extracting) the request params from the form
    const {
      company,
      website,
      location,
      bio,
      status,
      githubusername,
      skills,
      linkedin,
      instagram
    } = req.body;

    //create frofile fields based off schema
    const profileFields = {};

    profileFields.user = req.user.id;
    if (company) profileFields.company = company;
    if (website) profileFields.website = website;
    if (location) profileFields.location = location;
    if (bio) profileFields.bio = bio;
    if (status) profileFields.status = status;
    if (githubusername) profileFields.githubusername = githubusername;

    if (skills) {
      profileFields.skills = skills.split(",").map(skill => skill.trim());
    }

    //Build social object
    profileFields.social = {};
    if (linkedin) profileFields.social.linkedin = linkedin;
    if (instagram) profileFields.social.linkedin = instagram;

    //now try to update or make new profile (remember async/await)
    try {
      let profile = await Profile.findOne({ user: req.user.id });

      if (profile) {
        //Update
        profile = await Profile.findOneAndUpdate(
          { user: req.user.id },
          { $set: profileFields },
          { new: true }
        );
        return res.json(profile);
      }

      //Create
      profile = new Profile(profileFields);

      await profile.save();
      res.json(profile);

    } catch (err) {
      res.status(500).send("Server Error");
    }
  }
);



// @router    GET api/profile
// @desc      Get all profiles in an array of object
// @access    Public
router.get('/', async (req, res) => {
  try {

    //grab all profiles and populate the 'user' field with the req (url response) suer_id and gravatar (look more in to this)
    const profiles = await Profile.find().populate('user', ['name', 'avatar']);
    res.json(profiles);



  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});




// @router    GET api/profile/user/:user_id
// @desc      Get profile by USER id (the id inside the user object inside the whole profile model)
// @access    Public : no need for token because it takes in the use id
router.get('/user/:user_id', async (req, res) => {
  try {

    //grab profile user_id via the url (req object passed in requestr) and again populate the output with this user objects sent in userid 
    const profile = await Profile.findOne({ user: req.params.user_id }).populate('user', ['name', 'avatar']);

    //if the profile does not exist
    if (!profile) {
      return res.status(400).json({ msg: 'Profile not found' });
    }

    //else send the response of the profiles object (remember it )
    res.json(profile);

  } catch (err) {
    console.error(err.message);
    if (err.kind == 'ObjectID') {
      return res.status(400).json({ msg: 'Profile not found' });
    }
    res.status(500).send('Server Error');
  }
});



// @router    DELETE api/profile
// @desc      Delete whole user profile (profile, posts, etc.)
// @access    Private (need token)
router.delete('/', auth, async (req, res) => {
  try {

    //@todo - remove user posts

    //grab profile being requested to delete
    await Profile.findOneAndRemove({ user: req.user.id });  //notice the param of the model function is the field to search in that model! user is a field in Profile

    //Remove the user from the system completely
    await User.findOneAndRemove({ _id: req.user.id });      //and _id is field for the User which is unique, and sent as a token for private requests

    //message
    res.json({ msg: 'User Deleted' });

  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});


//IGNORE: IF WE WISH TO ADD WORK HISTORY, THIS WOULD BE IT
// // @router    PUT api/profile/experience
// // @desc      Adding experiences to profile (like work experience)
// // @access    Private (need token) and we need to authenticate the inputs, so, run checks for our second middleware
// router.put('/experience', [auth, [
//   check('title', 'Title is required')
//     .not()
//     .isEmpty(),
//   check('company', 'Company is required')
//     .not()
//     .isEmpty(),
//   check('from', 'From is required')
//     .not()
//     .isEmpty(),
// ]], async (req, res) => {

//   //validate any errors
//   const errors = validationResult(req);
//   if (!errors.isEmpty()) {
//     return res.status(400).json({ errors: errors.array() });
//   }

//   //destructure the request (the request is a form of input for experiences on the front end, so pull out each) from req.body
//   const {
//     title,
//     company,
//     location,
//     from,
//     to,
//     current,
//     descritpion
//   } = req.body;

//   //we destrucutred (pulled out from some object), so we can simply pass it to the new experiences object in which we will add to out array of experiences for the profile model! The array is an array of objects
//   const newExperience = {
//     title,
//     company,
//     location,
//     from,
//     to,
//     current,
//     descritpion
//   };

//   try {

//     //grab the requested profile via the token
//     const profile = await Profile.findOne({ user: req.user.id });

//     //push to array of experiences (but push to fron to mimick 'most recent') so use unshift(obj)
//     profile.experience.unshift(newExperience);

//     //save to db
//     await profile.save();

//     //send response
//     res.json(profile);

//   } catch (err) {
//     console.error(err.message);
//     res.status(500).send('Server Error');
//   }

//SCHOOL HISTORY ROUTE COULD BE ADDED TO MODEL/IMPLEMENTED HERE IF WANTED ---- IGNORE FOR NOW



// @router    GET api/profile/github/:username
// @desc      Get user repos from GithHub
// @access    Private (need token) and we need to authenticate the inputs, so, run checks for our second middleware
router.get('/github/:username', async (req, res) => {
  try {

    //grab username from request and the config files for github client id and secret
    const userName = req.params.username.toLowerCase();
    const clientId = config.get('githubClientId');
    const secret = config.get('githubSecret');

    //github option object: see docs
    const option = {
      uri: `https://api.github.com/users/${userName}/repos?per_page=5&sort=created:asc&client_id=${clientId}&client_secret=${secret}`,
      method: 'GET',
      headers: { 'user-agent': 'node.js' }
    };

    request(option, (error, response, body) => {
      if (error) console.error(error);
      if (response.statusCode !== 200) res.status(404).json({ msg: 'No Github Profile Found' });
      return res.json(JSON.parse(body));
    });

  } catch (err) {
    console.error(err);
    res.status(500).send('Server Error');
  }
});




module.exports = router;
